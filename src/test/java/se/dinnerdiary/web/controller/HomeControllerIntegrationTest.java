package se.dinnerdiary.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class HomeControllerIntegrationTest {

	private static final String VIEW = "/home";

	private MockMvc mockMvc;

	@InjectMocks
	private HomeController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		mockMvc = standaloneSetup(controller).build();

	}

	@Test
	public void rootUrlforwardsCorrectly() throws Exception {
		mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
				.andExpect(view().name(VIEW));
	}

}
